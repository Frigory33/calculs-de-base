#ifndef GLOBAL_H
#define GLOBAL_H


#include <stdio.h>
#include <math.h>

#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>


typedef struct GlobalData GlobalData;
struct GlobalData {
   float zoom;
   int font_size, mid_font_size, big_font_size;
   ALLEGRO_FONT * font, * bold_font, * mid_font, * big_font;
   ALLEGRO_DISPLAY * disp;
   ALLEGRO_BITMAP * disp_icon;
   ALLEGRO_EVENT_QUEUE * evt_queue;

   bool quit;
};

typedef struct Scrolling Scrolling;
struct Scrolling {
   int max_x, max_y;
   bool is_scrolling;
   int dx, dy;
};


extern GlobalData glob;


void init_scrolling(Scrolling * scroll);
void calibrate_scrolling(Scrolling * scroll);

void manage_global_events(ALLEGRO_EVENT evt, Scrolling * scroll);

void draw_cursor(double cursor_blinking_start, float x, float y);


#endif
