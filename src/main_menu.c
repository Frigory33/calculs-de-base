#include "global.h"
#include "menus.h"
#include "fraction_numbers.h"


typedef struct MainMenuData MainMenuData;
struct MainMenuData {
   MenuData menu_data;
   int random_int;
   double random_float;
   Fraction random_fraction;
};

MainMenuData main_menu_data;


float get_integer_grid_height(float width, int nb_cols, int nb_rows, int sign);
void draw_integer_grid(float x, float y, float width, int nb, int nb_cols, int nb_rows, int sign);
void draw_floating_horizontal_axis(float x, float y, float width, double max_nb, double nb, bool show_nb);
void draw_fraction(float x, float y, Fraction frac);

void draw_main_menu_item(int item_index)
{
   MenuData * menu_data = &main_menu_data.menu_data;
   float x = menu_data->items_width * item_index;
   switch (item_index) {
      case 0:
         int number_height = get_integer_grid_height(menu_data->items_width * .9, 10, 5, 0);
         draw_integer_grid(x + menu_data->items_width * .05,
            al_get_display_height(glob.disp) / 2. - number_height, menu_data->items_width * .9, main_menu_data.random_int,
            10, 5, 0);
         break;
      case 1:
         draw_floating_horizontal_axis(x + menu_data->items_width * .05, al_get_display_height(glob.disp) / 2. - glob.zoom * .8,
            menu_data->items_width * .9, 100., main_menu_data.random_float, true);
         break;
      case 2:
         draw_fraction(x + menu_data->items_width / 2., al_get_display_height(glob.disp) / 2. - glob.mid_font_size * 2.,
            main_menu_data.random_fraction);
   }
}


void integers_menu();
void floatings_menu();
void fractions_menu();

void main_menu()
{
   char const * menu_items_names[] = {
      "Nombres entiers relatifs",
      "Nombres à virgule",
      "Fractions",
   };

   MenuData * menu_data = &main_menu_data.menu_data;
   menu_data->nb_items = 3;
   menu_data->items_names = menu_items_names;
   menu_data->draw_item = &draw_main_menu_item;
   init_scrolling(&menu_data->scroll);
   menu_data->cur_selected_item = -1;
   main_menu_data.random_int = rand() * 101. / (RAND_MAX + 1.) - 50;
   main_menu_data.random_float = round(rand() * 20001. / (RAND_MAX + 1.)) / 100. - 100.;
   main_menu_data.random_fraction.numerator = rand() * 99. / (RAND_MAX + 1.) + 1.;
   main_menu_data.random_fraction.denominator = rand() * 99. / (RAND_MAX + 1.) + 1.;

   bool show_help = false;
   while (!glob.quit) {
      menu_data->items_width = glob.zoom * 2.;
      menu_data->scroll.max_x = ceilf(menu_data->nb_items * menu_data->items_width);
      menu_data->scroll.max_y = al_get_display_height(glob.disp);
      calibrate_scrolling(&menu_data->scroll);

      al_clear_to_color(al_map_rgb_f(1., 1., 1.));
      draw_menu(*menu_data);
      draw_help_indication(1, &menu_data->scroll);
      if (show_help) {
         draw_menu_help(&menu_data->scroll);
      }
      al_flip_display();

      ALLEGRO_EVENT evt;
      al_wait_for_event(glob.evt_queue, &evt);
      do {
         if (show_help) {
            switch (evt.type) {
               case ALLEGRO_EVENT_KEY_CHAR:
               case ALLEGRO_EVENT_MOUSE_BUTTON_UP:
                  show_help = false;
                  break;
            }
         } else {
            switch (evt.type) {
               case ALLEGRO_EVENT_KEY_CHAR:
                  switch (evt.keyboard.keycode) {
                     case ALLEGRO_KEY_F1:
                        menu_data->cur_selected_item = -1;
                        show_help = true;
                        break;
                  }
                  break;
               case ALLEGRO_EVENT_MOUSE_BUTTON_UP:
                  if (evt.mouse.button == 1) {
                     switch (menu_data->cur_selected_item) {
                        case 0:
                           integers_menu();
                           break;
                        case 1:
                           floatings_menu();
                           break;
                        case 2:
                           fractions_menu();
                           break;
                     }
                  }
                  break;
               case ALLEGRO_EVENT_MOUSE_AXES:
                  menu_data->cur_selected_item =
                     (evt.mouse.x + menu_data->scroll.dx) / menu_data->items_width;
                  break;
            }
         }
         manage_global_events(evt, &menu_data->scroll);
      } while (al_get_next_event(glob.evt_queue, &evt));
   }
}
