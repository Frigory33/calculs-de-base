#ifndef MENUS_H
#define MENUS_H


typedef struct MenuData MenuData;
struct MenuData {
   int nb_items;
   char const * * items_names;
   void (* draw_item)(int item_index);
   float items_width;
   Scrolling scroll;
   int cur_selected_item;
};


void draw_menu(MenuData menu_data);
void draw_menu_help(Scrolling const * scroll);
void draw_application_help(Scrolling const * scroll);
void draw_help_indication(int number, Scrolling const * scroll);


#endif
