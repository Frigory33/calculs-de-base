#include "global.h"
#include "menus.h"


void draw_floating_horizontal_axis(float x, float y, float width, double max_nb, double nb, bool show_nb)
{
   float line_thickness = glob.zoom * .04, max_nb_length = width / 2. - line_thickness * 4.;
   float nb_position = x + width / 2. + nb * max_nb_length / max_nb;
   al_draw_line(x + width / 2., y + line_thickness * 2., nb_position, y + line_thickness * 2.,
      al_map_rgb_f(0., 0., 0.), line_thickness);
   al_draw_line(x + width / 2., y, x + width / 2., y + line_thickness * 4., al_map_rgb_f(0., 0., 0.), line_thickness / 2.);
   al_draw_filled_triangle(nb_position + line_thickness * 2. * (nb < 0. ? -1. : 1.), y + line_thickness * 2., nb_position, y,
      nb_position, y + line_thickness * 4., al_map_rgb_f(0., 0., 0.));
   al_draw_textf(glob.font, al_map_rgb_f(0., 0., 0.), nb_position, y + line_thickness * 4.,
      ALLEGRO_ALIGN_CENTRE, show_nb ? "%lg" : "?", nb);
}

void draw_floating_area(float x, float y, float width, double max_nb, double nb1, double nb2, bool show_nb2, bool show_product)
{
   float line_thickness = glob.zoom * .04, max_nb_length = width / 2. - line_thickness * 4.;
   float nb1_position = x + width / 2. + nb1 * max_nb_length / max_nb,
      nb2_position = y + width / 2. + nb2 * max_nb_length / max_nb;
   al_draw_filled_rectangle(x + width / 2., y + width / 2., nb1_position, nb2_position, al_map_rgb_f(.8, 1., .8));
   al_draw_line(x + width / 2., y + width / 2., nb1_position, y + width / 2., al_map_rgb_f(0., 0., 0.), line_thickness);
   al_draw_filled_triangle(nb1_position + line_thickness * 2. * (nb1 < 0. ? -1. : 1.), y + width / 2.,
      nb1_position, y + width / 2. - line_thickness * 2., nb1_position, y + width / 2. + line_thickness * 2.,
      al_map_rgb_f(0., 0., 0.));
   al_draw_textf(glob.font, al_map_rgb_f(0., 0., 0.),
      nb1_position, y + width / 2. + (nb2 < 0. ? line_thickness * 2.5 : -line_thickness * 2.5 - glob.font_size),
      ALLEGRO_ALIGN_CENTRE, "%lg", nb1);
   al_draw_line(x + width / 2., y + width / 2., x + width / 2., nb2_position, al_map_rgb_f(0., 0., 0.), line_thickness);
   al_draw_filled_triangle(x + width / 2., nb2_position + line_thickness * 2. * (nb2 < 0. ? -1. : 1.),
      x + width / 2. - line_thickness * 2., nb2_position, x + width / 2. + line_thickness * 2., nb2_position,
      al_map_rgb_f(0., 0., 0.));
   al_draw_textf(glob.font, al_map_rgb_f(0., 0., 0.),
      x + width / 2. + line_thickness * 2.5 * (nb1 < 0. ? 1. : -1.), nb2_position - glob.font_size / 2.,
      nb1 < 0. ? ALLEGRO_ALIGN_LEFT : ALLEGRO_ALIGN_RIGHT, show_nb2 ? "%lg" : "?", nb2);
   al_draw_textf(glob.font, al_map_rgb_f(0., 0., 0.),
      (x + width / 2. + nb1_position) / 2., (y + width / 2. + nb2_position) / 2. - glob.font_size / 2.,
      ALLEGRO_ALIGN_CENTRE, show_product ? "%lg" : "?", nb1 * nb2);
}


typedef struct FloatingScreenData FloatingScreenData;
struct FloatingScreenData {
   double (* operation)(double nb1, double nb2);
   char const * op_text;
   int nb_nbs, nb_result_steps;
   bool generate_negative;
   double precision, limit, limit2, min_dist_from_zero;

   double nbs[5], result;
   char nbs_texts[5][12];
   int show_result_step;
   int cursor_pos;
   double cursor_blinking_start;
   bool show_axes;
};

void init_floatings_data(FloatingScreenData * data)
{
   for (int i = 0; i < data->nb_nbs; ++i) {
      double limit = i == 0 || data->limit2 == 0. ? data->limit : data->limit2;
      do {
         if (data->generate_negative) {
            data->nbs[i] = round(rand() * (limit / data->precision * 2. + 1.) / (RAND_MAX + 1.)) * data->precision - limit;
         } else {
            data->nbs[i] = round(rand() * (limit / data->precision + 1.) / (RAND_MAX + 1.)) * data->precision;
         }
      } while (fabs(data->nbs[i]) < data->min_dist_from_zero);
      sprintf(data->nbs_texts[i], "%.*lf", (int)-log10(data->precision), data->nbs[i]);
   }
   data->result = data->nbs[0];
   for (int i = 1; i < data->nb_nbs; ++i) {
      data->result = data->operation(data->result, data->nbs[i]);
   }
   data->show_result_step = 0;
   data->cursor_pos = -1;
}

void apply_floating_value(FloatingScreenData * data)
{
   if (data->cursor_pos >= 0) {
      data->nbs[data->cursor_pos] = atof(data->nbs_texts[data->cursor_pos]);
      int limit = data->cursor_pos == 0 || data->limit2 == 0. ? data->limit : data->limit2;
      if (data->nbs[data->cursor_pos] < -limit) {
         data->nbs[data->cursor_pos] = -limit;
      } else if (data->nbs[data->cursor_pos] > limit) {
         data->nbs[data->cursor_pos] = limit;
      } else if (abs(data->nbs[data->cursor_pos]) < data->min_dist_from_zero) {
         data->nbs[data->cursor_pos] = data->min_dist_from_zero;
      }
      sprintf(data->nbs_texts[data->cursor_pos], "%.*lf", (int)-log10(data->precision), data->nbs[data->cursor_pos]);
      data->result = data->nbs[0];
      for (int i = 1; i < data->nb_nbs; ++i) {
         data->result = data->operation(data->result, data->nbs[i]);
      }
   }
}


double float_addition_op(double nb1, double nb2)
{
   return nb1 + nb2;
}
double float_substraction_op(double nb1, double nb2)
{
   return nb1 - nb2;
}
double float_multiplication_op(double nb1, double nb2)
{
   return nb1 * nb2;
}
double float_division_op(double nb1, double nb2)
{
   return nb1 / nb2;
}
double float_cross_product_op(double nb1, double nb2)
{
   static int step = 1;
   step = (step + 1) % 2;
   return step == 0 ? nb2 / nb1 : nb1 * nb2;
}


void draw_vertical_float_operation(FloatingScreenData * data, float left_x)
{
   float line_height = 1.5, right_x = left_x + glob.zoom * 1.6;
   for (int i = 0; i < data->nb_nbs; ++i) {
      float y = al_get_display_height(glob.disp) / 2. + glob.mid_font_size * line_height * (i - (data->nb_nbs + 1) / 2.);
      al_draw_text(glob.mid_font, al_map_rgb_f(0., 0., 0.), right_x, y, ALLEGRO_ALIGN_RIGHT, data->nbs_texts[i]);
      if (i >= 1) {
         al_draw_text(glob.mid_font, al_map_rgb_f(0., 0., 0.), left_x, y, ALLEGRO_ALIGN_LEFT, data->op_text);
      }
      if (data->cursor_pos == i) {
         draw_cursor(data->cursor_blinking_start, right_x, y);
      }
   }
   float result_y = al_get_display_height(glob.disp) / 2. + glob.mid_font_size * line_height * (data->nb_nbs / 2. - .5),
      line_y = result_y - glob.mid_font_size * (line_height - 1.) / 2.;
   al_draw_line(left_x - glob.zoom * .05, line_y, right_x + glob.zoom * .05, line_y, al_map_rgb_f(0., 0., 0.), glob.zoom * .02);
   al_draw_textf(glob.mid_font, al_map_rgb_f(0., 0., 0.), right_x, result_y,
      ALLEGRO_ALIGN_RIGHT, data->show_result_step >= 1 ? "%.2lf" : "?", data->result);
}
void draw_centered_vertical_float_operation(FloatingScreenData * data)
{
   draw_vertical_float_operation(data, glob.zoom * 3.);
}

void draw_float_addition(FloatingScreenData * data)
{
   if (data->show_axes) {
      draw_floating_horizontal_axis(glob.zoom * .4, al_get_display_height(glob.disp) * .4, glob.zoom * 4.6, 100., data->nbs[0],
         true);
      draw_floating_horizontal_axis(glob.zoom * .4, al_get_display_height(glob.disp) * .5, glob.zoom * 4.6, 100., data->nbs[1],
         true);
      draw_floating_horizontal_axis(glob.zoom * .4, al_get_display_height(glob.disp) * .6, glob.zoom * 4.6, 100., data->result,
         data->show_result_step >= 1);
   }
   draw_vertical_float_operation(data, glob.zoom * (data->show_axes ? 5.6 : 3.));
}

void draw_float_round(FloatingScreenData * data)
{
   float line_height = 1.5, y = al_get_display_height(glob.disp) / 2. - glob.mid_font_size * line_height * 2.;
   al_draw_multiline_textf(glob.mid_font, al_map_rgb_f(0., 0., 0.), glob.zoom * 3.2, y, glob.zoom,
      glob.mid_font_size * line_height, ALLEGRO_ALIGN_LEFT,
      data->show_result_step >= 3 ? "%s\n%.2lf\n%.1lf\n%.0lf" :
         (data->show_result_step >= 2 ? "%s\n%.2lf\n%.1lf\n?" :
         (data->show_result_step >= 1 ? "%s\n%.2lf\n?\n?" : "%s\n?\n?\n?")),
      data->nbs_texts[0], round(data->nbs[0] * 100.) / 100., round(data->nbs[0] * 10.) / 10., round(data->nbs[0]));
   if (data->cursor_pos == 0) {
      draw_cursor(data->cursor_blinking_start, glob.zoom * 3.2 + al_get_text_width(glob.mid_font, data->nbs_texts[0]), y);
   }
}

void draw_float_multiplication(FloatingScreenData * data)
{
   if (data->show_axes) {
      draw_floating_area(glob.zoom * .4, glob.zoom * .4, glob.zoom * 4.6, 10., data->nbs[0], data->nbs[1], true,
         data->show_result_step >= 1);
   }
   draw_vertical_float_operation(data, glob.zoom * (data->show_axes ? 5.6 : 3.));
}

void draw_float_division(FloatingScreenData * data)
{
   if (data->show_axes) {
      draw_floating_area(glob.zoom * .4, glob.zoom * .4, glob.zoom * 4.6, 10., data->nbs[1], data->result,
         data->show_result_step >= 1, true);
   }
   draw_vertical_float_operation(data, glob.zoom * (data->show_axes ? 5.6 : 3.));
}

void draw_cross_product_table(FloatingScreenData * data)
{
   float text_x[] = { glob.zoom * 3., glob.zoom * 4.6, glob.zoom * 3. },
      text_y[] = { -glob.mid_font_size * 2., -glob.mid_font_size * 2., 0. };
   for (int i = 0; i < 3; ++i) {
      float y = al_get_display_height(glob.disp) / 2. + text_y[i];
      al_draw_text(glob.mid_font, al_map_rgb_f(0., 0., 0.), text_x[i], y, ALLEGRO_ALIGN_CENTRE, data->nbs_texts[i]);
      if (data->cursor_pos == i) {
         draw_cursor(data->cursor_blinking_start, text_x[i] + al_get_text_width(glob.mid_font, data->nbs_texts[i]) / 2., y);
      }
   }
   for (int i = 0; i < 3; ++i) {
      float hor_line_y = al_get_display_height(glob.disp) / 2. + glob.mid_font_size * (i * 2. - 2.5),
         ver_line_x = glob.zoom * 2.2 + i * glob.zoom * 1.6;
      al_draw_line(glob.zoom * 2.2, hor_line_y, glob.zoom * 5.4, hor_line_y, al_map_rgb_f(0., 0., 0.), glob.zoom * .02);
      al_draw_line(ver_line_x, al_get_display_height(glob.disp) / 2. - glob.mid_font_size * 2.5,
         ver_line_x, al_get_display_height(glob.disp) / 2. + glob.mid_font_size * 1.5, al_map_rgb_f(0., 0., 0.), glob.zoom * .02);
   }
   al_draw_textf(glob.mid_font, al_map_rgb_f(0., 0., 0.),
      glob.zoom * 4.6, al_get_display_height(glob.disp) / 2., ALLEGRO_ALIGN_CENTRE,
      data->show_result_step >= 1 ? "%lg" : "?", data->result);
}


void floatings_operation_screen(FloatingScreenData * data, void (* draw_fct)(FloatingScreenData * data))
{
   Scrolling scroll;
   init_scrolling(&scroll);

   init_floatings_data(data);
   data->show_axes = true;

   bool get_back = false, show_help = false;
   while (!glob.quit && !get_back) {
      scroll.max_x = ceilf(glob.zoom * 7.6);
      scroll.max_y = al_get_display_height(glob.disp);
      calibrate_scrolling(&scroll);

      al_clear_to_color(al_map_rgb_f(1., 1., 1.));
      draw_fct(data);
      draw_help_indication(2, &scroll);
      if (show_help) {
         draw_application_help(&scroll);
      }
      al_flip_display();

      ALLEGRO_EVENT evt;
      bool is_event_there = al_wait_for_event_timed(glob.evt_queue, &evt, .1);
      while (is_event_there) {
         if (show_help) {
            switch (evt.type) {
               case ALLEGRO_EVENT_KEY_CHAR:
               case ALLEGRO_EVENT_MOUSE_BUTTON_UP:
                  show_help = false;
                  break;
            }
         } else {
            switch (evt.type) {
               case ALLEGRO_EVENT_KEY_CHAR:
                  switch (evt.keyboard.keycode) {
                     case ALLEGRO_KEY_ESCAPE:
                        get_back = true;
                        break;
                     case ALLEGRO_KEY_F1:
                        show_help = true;
                        break;
                     case ALLEGRO_KEY_F2:
                        init_floatings_data(data);
                        break;
                     case ALLEGRO_KEY_F3:
                        data->generate_negative = !data->generate_negative;
                        init_floatings_data(data);
                        break;
                     case ALLEGRO_KEY_F4:
                        data->show_axes = !data->show_axes;
                        break;
                     case ALLEGRO_KEY_SPACE:
                        if (evt.keyboard.modifiers & ALLEGRO_KEYMOD_SHIFT) {
                           data->show_result_step = (data->show_result_step + data->nb_result_steps - 1) % data->nb_result_steps;
                        } else {
                           data->show_result_step = (data->show_result_step + 1) % data->nb_result_steps;
                        }
                        break;
                     case ALLEGRO_KEY_TAB:
                        apply_floating_value(data);
                        if (evt.keyboard.modifiers & ALLEGRO_KEYMOD_SHIFT) {
                           data->cursor_pos = (data->cursor_pos + data->nb_nbs - 1) % data->nb_nbs;
                        } else {
                           data->cursor_pos = (data->cursor_pos + 1) % data->nb_nbs;
                        }
                        data->cursor_blinking_start = al_get_time();
                        break;
                     case ALLEGRO_KEY_ENTER:
                        apply_floating_value(data);
                        data->cursor_pos = -1;
                        break;
                     default:
                        if (data->cursor_pos >= 0) {
                           char * modified_text = data->nbs_texts[data->cursor_pos];
                           int text_length = strlen(modified_text);
                           if (evt.keyboard.keycode == ALLEGRO_KEY_BACKSPACE) {
                              if (text_length > 0) {
                                 modified_text[text_length - 1] = '\0';
                              }
                           } else if (evt.keyboard.unichar == '-' && text_length == 0) {
                              strcpy(modified_text, "-");
                           } else if (evt.keyboard.unichar == ',' && strchr(modified_text, ',') == NULL) {
                              strcat(modified_text, ",");
                           } else if ('0' <= evt.keyboard.unichar && evt.keyboard.unichar <= '9' && text_length < 7) {
                              sprintf(modified_text + text_length, "%c", evt.keyboard.unichar);
                           }
                           data->cursor_blinking_start = al_get_time();
                        }
                  }
                  break;
            }
         }
         manage_global_events(evt, &scroll);
         is_event_there = al_get_next_event(glob.evt_queue, &evt);
      }
   }
}


MenuData floatings_menu_data;

void draw_floatings_menu_item(int item_index)
{
   char const * symbols[] = { "+", "−", ",", "×", "÷", "+", "×÷" };
   al_draw_text(glob.big_font, al_map_rgb_f(0., 0., 0.),
      floatings_menu_data.items_width * (item_index * 2 + 1) / 2., al_get_display_height(glob.disp) / 2. - glob.big_font_size,
      ALLEGRO_ALIGN_CENTRE, symbols[item_index]);
}

void floatings_menu()
{
   char const * menu_items_names[] = {
      "Addition",
      "Soustraction",
      "Arrondi",
      "Multiplication",
      "Division",
      "Addition de 5 nombres",
      "Produit en croix",
   };

   floatings_menu_data.nb_items = sizeof(menu_items_names) / sizeof(*menu_items_names);
   floatings_menu_data.items_names = menu_items_names;
   floatings_menu_data.draw_item = &draw_floatings_menu_item;
   init_scrolling(&floatings_menu_data.scroll);
   floatings_menu_data.cur_selected_item = -1;

   bool get_back = false, show_help = false;
   while (!glob.quit && !get_back) {
      floatings_menu_data.items_width = glob.zoom * 2.;
      floatings_menu_data.scroll.max_x = ceilf(floatings_menu_data.nb_items * floatings_menu_data.items_width);
      floatings_menu_data.scroll.max_y = al_get_display_height(glob.disp);
      calibrate_scrolling(&floatings_menu_data.scroll);

      al_clear_to_color(al_map_rgb_f(1., 1., 1.));
      draw_menu(floatings_menu_data);
      draw_help_indication(1, &floatings_menu_data.scroll);
      if (show_help) {
         draw_menu_help(&floatings_menu_data.scroll);
      }
      al_flip_display();

      ALLEGRO_EVENT evt;
      al_wait_for_event(glob.evt_queue, &evt);
      do {
         if (show_help) {
            switch (evt.type) {
               case ALLEGRO_EVENT_KEY_CHAR:
               case ALLEGRO_EVENT_MOUSE_BUTTON_UP:
                  show_help = false;
                  break;
            }
         } else {
            switch (evt.type) {
               case ALLEGRO_EVENT_KEY_CHAR:
                  switch (evt.keyboard.keycode) {
                     case ALLEGRO_KEY_ESCAPE:
                        get_back = true;
                        break;
                     case ALLEGRO_KEY_F1:
                        floatings_menu_data.cur_selected_item = -1;
                        show_help = true;
                        break;
                  }
                  break;
               case ALLEGRO_EVENT_MOUSE_BUTTON_UP:
                  if (evt.mouse.button == 1) {
                     switch (floatings_menu_data.cur_selected_item) {
                        case 0: {
                           FloatingScreenData data = { &float_addition_op, "+", 2, 2, false, .01, 50. };
                           floatings_operation_screen(&data, &draw_float_addition);
                           break;
                        }
                        case 1: {
                           FloatingScreenData data = { &float_substraction_op, "−", 2, 2, false, .01, 50. };
                           floatings_operation_screen(&data, &draw_float_addition);
                           break;
                        }
                        case 2: {
                           FloatingScreenData data = { NULL, ",", 1, 4, false, .001, 100. };
                           floatings_operation_screen(&data, &draw_float_round);
                           break;
                        }
                        case 3: {
                           FloatingScreenData data = { &float_multiplication_op, "×", 2, 2, false, .01, 10. };
                           floatings_operation_screen(&data, &draw_float_multiplication);
                           break;
                        }
                        case 4: {
                           FloatingScreenData data = { &float_division_op, "÷", 2, 2, false, .01, 80., 10., 5. };
                           floatings_operation_screen(&data, &draw_float_division);
                           break;
                        }
                        case 5: {
                           FloatingScreenData data = { &float_addition_op, "+", 5, 2, false, .01, 10. };
                           floatings_operation_screen(&data, &draw_centered_vertical_float_operation);
                           break;
                        }
                        case 6: {
                           FloatingScreenData data = { &float_cross_product_op, "+", 3, 2, false, .1, 10. };
                           floatings_operation_screen(&data, &draw_cross_product_table);
                           break;
                        }
                     }
                  }
                  break;
               case ALLEGRO_EVENT_MOUSE_AXES:
                  floatings_menu_data.cur_selected_item =
                     (evt.mouse.x + floatings_menu_data.scroll.dx) / floatings_menu_data.items_width;
                  break;
            }
         }
         manage_global_events(evt, &floatings_menu_data.scroll);
      } while (al_get_next_event(glob.evt_queue, &evt));
   }
}
