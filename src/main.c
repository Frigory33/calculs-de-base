#include <locale.h>

#include "global.h"


GlobalData glob;


void reload_fonts()
{
   al_destroy_font(glob.font);
   al_destroy_font(glob.bold_font);
   al_destroy_font(glob.mid_font);
   al_destroy_font(glob.big_font);
   glob.font_size = roundf(16 * glob.zoom / 100.);
   glob.mid_font_size = roundf(32 * glob.zoom / 100.);
   glob.big_font_size = roundf(48 * glob.zoom / 100.);
   glob.font = al_load_ttf_font("DejaVuSans.ttf", glob.font_size, 0);
   glob.bold_font = al_load_ttf_font("DejaVuSans-Bold.ttf", glob.font_size, 0);
   glob.mid_font = al_load_ttf_font("DejaVuSans.ttf", glob.mid_font_size, 0);
   glob.big_font = al_load_ttf_font("DejaVuSans.ttf", glob.big_font_size, 0);
}


void calibrate_scrolling(Scrolling * scroll)
{
   if (scroll->max_x < al_get_display_width(glob.disp)) {
      scroll->dx = (scroll->max_x - al_get_display_width(glob.disp)) / 2;
   } else if (scroll->dx + al_get_display_width(glob.disp) > scroll->max_x) {
      scroll->dx = scroll->max_x - al_get_display_width(glob.disp);
   } else if (scroll->dx < 0) {
      scroll->dx = 0;
   }
   if (scroll->max_y < al_get_display_height(glob.disp)) {
      scroll->dy = (scroll->max_y - al_get_display_height(glob.disp)) / 2;
   } else if (scroll->dy + al_get_display_height(glob.disp) > scroll->max_y) {
      scroll->dy = scroll->max_y - al_get_display_height(glob.disp);
   } else if (scroll->dy < 0) {
      scroll->dy = 0;
   }
   ALLEGRO_TRANSFORM trans;
   al_identity_transform(&trans);
   al_translate_transform(&trans, -scroll->dx, -scroll->dy);
   al_use_transform(&trans);
}

void init_scrolling(Scrolling * scroll)
{
   scroll->max_x = al_get_display_width(glob.disp);
   scroll->max_y = al_get_display_height(glob.disp);
   scroll->is_scrolling = false;
   scroll->dx = scroll->dy = 0;
}



void manage_global_events(ALLEGRO_EVENT evt, Scrolling * scroll)
{
   switch (evt.type) {
      case ALLEGRO_EVENT_DISPLAY_RESIZE:
         al_acknowledge_resize(glob.disp);
         break;
      case ALLEGRO_EVENT_DISPLAY_CLOSE:
         glob.quit = true;
         break;
      case ALLEGRO_EVENT_MOUSE_BUTTON_DOWN:
         if (evt.mouse.button == 2 && scroll != NULL) {
            scroll->is_scrolling = true;
            al_set_system_mouse_cursor(glob.disp, ALLEGRO_SYSTEM_MOUSE_CURSOR_MOVE);
         }
         break;
      case ALLEGRO_EVENT_MOUSE_BUTTON_UP:
         if (evt.mouse.button == 2 && scroll != NULL) {
            scroll->is_scrolling = false;
            al_set_system_mouse_cursor(glob.disp, ALLEGRO_SYSTEM_MOUSE_CURSOR_DEFAULT);
         }
         break;
      case ALLEGRO_EVENT_MOUSE_AXES:
         if (scroll != NULL && scroll->is_scrolling) {
            scroll->dx -= evt.mouse.dx;
            scroll->dy -= evt.mouse.dy;
         }
         if (evt.mouse.dz != 0) {
            glob.zoom += evt.mouse.dz * 4.;
            if (glob.zoom < 4.) {
               glob.zoom = 4.;
            }
            reload_fonts();
         }
         break;
   }
}


void draw_cursor(double cursor_blinking_start, float x, float y)
{
   if (fmod(al_get_time() - cursor_blinking_start, 1.0) < .5) {
      al_draw_line(x, y, x, y + glob.mid_font_size, al_map_rgb_f(0., 0., 0.), glob.zoom * .02);
   }
}


void init()
{
   setlocale(LC_ALL, "");

   srand(time(NULL));

   al_init();
   al_init_primitives_addon();
   al_init_image_addon();
   al_init_font_addon();
   al_init_ttf_addon();

   al_install_keyboard();
   al_install_mouse();

   glob.zoom = 100.;
   reload_fonts();

   al_set_new_display_flags(ALLEGRO_RESIZABLE);
   al_set_new_display_option(ALLEGRO_SAMPLE_BUFFERS, 1, ALLEGRO_SUGGEST);
   al_set_new_display_option(ALLEGRO_SAMPLES, 8, ALLEGRO_SUGGEST);
   al_set_new_window_title("Calculs de base");
   glob.disp = al_create_display(800, 600);
   glob.disp_icon = al_load_bitmap("icon.png");
   al_set_display_icon(glob.disp, glob.disp_icon);

   glob.evt_queue = al_create_event_queue();
   al_register_event_source(glob.evt_queue, al_get_display_event_source(glob.disp));
   al_register_event_source(glob.evt_queue, al_get_keyboard_event_source());
   al_register_event_source(glob.evt_queue, al_get_mouse_event_source());
}

void deinit()
{
   al_destroy_event_queue(glob.evt_queue);
   al_destroy_display(glob.disp);
   al_destroy_bitmap(glob.disp_icon);
   al_destroy_font(glob.font);
}


void main_menu();

int main()
{
   init();

   glob.quit = false;

   main_menu();

   deinit();
}
