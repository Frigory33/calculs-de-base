#include "global.h"
#include "menus.h"


float get_integer_grid_height(float width, int nb_cols, int nb_rows, int sign)
{
   float cell_side = width / (nb_cols + 2);
   return cell_side * nb_rows * (sign == 0 && nb_cols != 0 ? 2 : 1);
}

void draw_integer_grid(float x, float y, float width, int nb, int nb_cols, int nb_rows, int sign)
{
   float cell_side = width / (nb_cols + 2);
   for (int row = sign <= 0 ? -nb_rows : 0; row < (sign >= 0 ? nb_rows : 0); ++row) {
      for (int col = 0; col < nb_cols; ++col) {
         int cur_nb = row * nb_cols + col;
         ALLEGRO_COLOR color = al_map_rgb_f(.5, .5, .5);
         if (nb < 0 && cur_nb < 0 && nb <= cur_nb) {
            color = al_map_rgb_f(1., 0., 0.);
         } else if (nb >= 0 && cur_nb >= 0 && nb > cur_nb) {
            color = al_map_rgb_f(0., 1., 0.);
         }
         int actual_row = row + (sign <= 0 ? nb_rows : 0);
         al_draw_filled_rectangle(x + (col + 1) * cell_side, y + actual_row * cell_side,
            x + (col + 2) * cell_side, y + (actual_row + 1) * cell_side, color);
         al_draw_rectangle(x + (col + 1) * cell_side, y + actual_row * cell_side,
            x + (col + 2) * cell_side, y + (actual_row + 1) * cell_side, al_map_rgb_f(0., 0., 0.), cell_side / 5.);
      }
   }
   if (sign == 0) {
      if (nb_cols == 0) {
         nb_rows = 0;
      } else if (nb_rows == 0) {
         nb_cols = 0;
      }
      float separation_line_y = y + nb_rows * cell_side;
      al_draw_line(x, separation_line_y, x + (nb_cols + 2) * cell_side, separation_line_y,
         al_map_rgb_f(0., 0., 0.), cell_side / 5.);
   }
}


typedef struct IntegerScreenData IntegerScreenData;
struct IntegerScreenData {
   int (* operation)(int nb1, int nb2);
   char const * op_text;
   int nb_nbs, nb_result_steps;
   bool generate_negative;
   int limit, limit2, min_dist_from_zero;

   int nbs[5], result;
   char nbs_texts[5][8];
   int show_result_step;
   int cursor_pos;
   double cursor_blinking_start;
   bool show_grids;
};

void init_integers_data(IntegerScreenData * data)
{
   for (int i = 0; i < data->nb_nbs; ++i) {
      int limit = i == 0 || data->limit2 == 0 ? data->limit : data->limit2;
      do {
         if (data->generate_negative) {
            data->nbs[i] = rand() * (double)(limit * 2 + 1) / (RAND_MAX + 1.) - limit;
         } else {
            data->nbs[i] = rand() * (double)(limit + 1) / (RAND_MAX + 1.);
         }
      } while (abs(data->nbs[i]) < data->min_dist_from_zero);
      sprintf(data->nbs_texts[i], "%d", data->nbs[i]);
   }
   data->result = data->nbs[0];
   for (int i = 1; i < data->nb_nbs; ++i) {
      data->result = data->operation(data->result, data->nbs[i]);
   }
   data->show_result_step = 0;
   data->cursor_pos = -1;
}

void apply_integer_value(IntegerScreenData * data)
{
   if (data->cursor_pos >= 0) {
      data->nbs[data->cursor_pos] = atoi(data->nbs_texts[data->cursor_pos]);
      int limit = data->cursor_pos == 0 || data->limit2 == 0 ? data->limit : data->limit2;
      if (data->nbs[data->cursor_pos] < -limit) {
         data->nbs[data->cursor_pos] = -limit;
      } else if (data->nbs[data->cursor_pos] > limit) {
         data->nbs[data->cursor_pos] = limit;
      } else if (abs(data->nbs[data->cursor_pos]) < data->min_dist_from_zero) {
         data->nbs[data->cursor_pos] = data->min_dist_from_zero;
      }
      sprintf(data->nbs_texts[data->cursor_pos], "%d", data->nbs[data->cursor_pos]);
      data->result = data->nbs[0];
      for (int i = 1; i < data->nb_nbs; ++i) {
         data->result = data->operation(data->result, data->nbs[i]);
      }
   }
}


int int_addition_op(int nb1, int nb2)
{
   return nb1 + nb2;
}
int int_substraction_op(int nb1, int nb2)
{
   return nb1 - nb2;
}
int int_multiplication_op(int nb1, int nb2)
{
   return nb1 * nb2;
}
int int_division_op(int nb1, int nb2)
{
   return nb2 == 0 ? nb1 : nb1 / nb2;
}


void draw_int_operation_text(IntegerScreenData * data, float y)
{
   al_draw_text(glob.mid_font, al_map_rgb_f(0., 0., 0.), glob.zoom * 1.2, y, ALLEGRO_ALIGN_CENTRE,
      data->nbs_texts[0]);
   al_draw_text(glob.mid_font, al_map_rgb_f(0., 0., 0.), glob.zoom * 2.5, y, ALLEGRO_ALIGN_CENTRE, data->op_text);
   al_draw_text(glob.mid_font, al_map_rgb_f(0., 0., 0.), glob.zoom * 3.8, y, ALLEGRO_ALIGN_CENTRE,
      data->nbs_texts[1]);
   al_draw_text(glob.mid_font, al_map_rgb_f(0., 0., 0.), glob.zoom * 5.1, y, ALLEGRO_ALIGN_CENTRE, "=");
   al_draw_textf(glob.mid_font, al_map_rgb_f(0., 0., 0.), glob.zoom * 6.4, y, ALLEGRO_ALIGN_CENTRE,
      data->show_result_step >= 2 ? "%d" : "?", data->result);
   if (data->cursor_pos >= 0) {
      draw_cursor(data->cursor_blinking_start, glob.zoom * (data->cursor_pos == 0 ? 1.2 : 3.8) +
         al_get_text_width(glob.mid_font, data->nbs_texts[data->cursor_pos]) / 2., y);
   }
}

void draw_int_addition(IntegerScreenData * data)
{
   float figures_y = al_get_display_height(glob.disp) / 2. - glob.mid_font_size / 2.;
   if (data->show_grids) {
      int sign = data->generate_negative ? 0 : 1;
      int number_height = get_integer_grid_height(glob.zoom * 2., 10, 5, sign);
      draw_integer_grid(glob.zoom * .2, al_get_display_height(glob.disp) / 2. - number_height / 2., glob.zoom * 2.,
         data->nbs[0], 10, 5, sign);
      al_draw_text(glob.mid_font, al_map_rgb_f(0., 0., 0.),
         glob.zoom * 2.5, al_get_display_height(glob.disp) / 2. - glob.mid_font_size / 2., ALLEGRO_ALIGN_CENTRE, data->op_text);
      draw_integer_grid(glob.zoom * 2.8, al_get_display_height(glob.disp) / 2. - number_height / 2., glob.zoom * 2.,
         data->nbs[1], 10, 5, sign);
      al_draw_text(glob.mid_font, al_map_rgb_f(0., 0., 0.),
         glob.zoom * 5.1, al_get_display_height(glob.disp) / 2. - glob.mid_font_size / 2., ALLEGRO_ALIGN_CENTRE, "=");
      int result_height = get_integer_grid_height(glob.zoom * 2., 10, 10, data->generate_negative ? 0 : data->result);
      if (data->show_result_step >= 1) {
         draw_integer_grid(glob.zoom * 5.4, al_get_display_height(glob.disp) / 2. - result_height / 2., glob.zoom * 2.,
            data->result, 10, 10, data->generate_negative ? 0 : data->result);
      } else {
         al_draw_text(glob.mid_font, al_map_rgb_f(0., 0., 0.),
            glob.zoom * 6.4, al_get_display_height(glob.disp) / 2. - glob.mid_font_size / 2., ALLEGRO_ALIGN_CENTRE, "?");
      }
      figures_y = al_get_display_height(glob.disp) / 2. + result_height / 2. + glob.zoom * .2;
   }
   draw_int_operation_text(data, figures_y);
}

void draw_int_multiplication(IntegerScreenData * data)
{
   float figures_y = al_get_display_height(glob.disp) / 2. - glob.mid_font_size / 2.;
   if (data->show_grids) {
      float cell_side = glob.zoom * .18;
      int absol_nbs[2] = { abs(data->nbs[0]), abs(data->nbs[1]) };
      int result_height = get_integer_grid_height(cell_side * (absol_nbs[0] + 2),
         absol_nbs[0], (absol_nbs[1] == 0 ? 1 : absol_nbs[1]), data->result);
      draw_integer_grid(glob.zoom * 1.2 - cell_side * (absol_nbs[0] + 2) / 2.,
         al_get_display_height(glob.disp) / 2. - result_height,
         cell_side * (absol_nbs[0] + 2), data->nbs[0], absol_nbs[0], 1, data->nbs[0]);
      al_draw_text(glob.mid_font, al_map_rgb_f(0., 0., 0.),
         glob.zoom * 2.5, al_get_display_height(glob.disp) / 2. - result_height / 2. - glob.mid_font_size / 2.,
         ALLEGRO_ALIGN_CENTRE, data->op_text);
      draw_integer_grid(glob.zoom * 3.8 - (cell_side * (data->nbs[1] == 0 ? 2 : 3)) / 2.,
         al_get_display_height(glob.disp) / 2. - result_height,
         cell_side * 3, data->nbs[1], 1, absol_nbs[1], data->nbs[1]);
      al_draw_text(glob.mid_font, al_map_rgb_f(0., 0., 0.),
         glob.zoom * 5.1, al_get_display_height(glob.disp) / 2. - result_height / 2. - glob.mid_font_size / 2.,
         ALLEGRO_ALIGN_CENTRE, "=");
      if (data->show_result_step >= 1) {
         draw_integer_grid(glob.zoom * 6.4 - cell_side * (data->result == 0 ? 2 : absol_nbs[0] + 2) / 2.,
            al_get_display_height(glob.disp) / 2. - result_height,
            cell_side * (absol_nbs[0] + 2), data->result, absol_nbs[0], absol_nbs[1], data->result);
      } else {
         al_draw_text(glob.mid_font, al_map_rgb_f(0., 0., 0.),
            glob.zoom * 6.4, al_get_display_height(glob.disp) / 2. - result_height / 2. - glob.mid_font_size / 2.,
            ALLEGRO_ALIGN_CENTRE, "?");
      }
      figures_y = al_get_display_height(glob.disp) / 2. + glob.zoom * .5;
   }
   draw_int_operation_text(data, figures_y);
}

void draw_int_division(IntegerScreenData * data)
{
   float figures_y = al_get_display_height(glob.disp) / 2. - glob.mid_font_size / 2.;
   if (data->show_grids) {
      float cell_side = glob.zoom * .18;
      int absol_nbs[2] = { abs(data->nbs[0]), abs(data->nbs[1]) };
      int numerator_height = get_integer_grid_height(cell_side * (absol_nbs[1] + 2),
         absol_nbs[1], int_division_op(absol_nbs[0] - 1, absol_nbs[1]) + 1, data->nbs[0]);
      draw_integer_grid(glob.zoom * 1.2 - cell_side * (absol_nbs[1] + 2) / 2.,
         al_get_display_height(glob.disp) / 2. - numerator_height,
         cell_side * (absol_nbs[1] + 2), data->nbs[0], absol_nbs[1], int_division_op(absol_nbs[0] - 1, absol_nbs[1]) + 1,
         data->nbs[0]);
      al_draw_text(glob.mid_font, al_map_rgb_f(0., 0., 0.),
         glob.zoom * 2.5, al_get_display_height(glob.disp) / 2. - numerator_height / 2. - glob.mid_font_size / 2.,
         ALLEGRO_ALIGN_CENTRE, data->op_text);
      draw_integer_grid(glob.zoom * 3.8 - (cell_side * (absol_nbs[1] + 2)) / 2.,
         al_get_display_height(glob.disp) / 2. - numerator_height / 2. - cell_side / 2.,
         cell_side * (absol_nbs[1] + 2), data->nbs[1], absol_nbs[1], 1, data->nbs[1]);
      al_draw_text(glob.mid_font, al_map_rgb_f(0., 0., 0.),
         glob.zoom * 5.1, al_get_display_height(glob.disp) / 2. - numerator_height / 2. - glob.mid_font_size / 2.,
         ALLEGRO_ALIGN_CENTRE, "=");
      if (data->show_result_step >= 1) {
         draw_integer_grid(glob.zoom * 6.4 - cell_side * 3. / 2.,
            al_get_display_height(glob.disp) / 2. - numerator_height +
               (data->nbs[1] != 0 && data->nbs[0] % data->nbs[1] == 0 ? 0. : cell_side / 2.),
            cell_side * 3, data->result, 1, abs(data->result), data->result);
      } else {
         al_draw_text(glob.mid_font, al_map_rgb_f(0., 0., 0.),
            glob.zoom * 6.4, al_get_display_height(glob.disp) / 2. - numerator_height / 2. - cell_side / 2.,
            ALLEGRO_ALIGN_CENTRE, "?");
      }
      figures_y = al_get_display_height(glob.disp) / 2. + glob.zoom * .5;
   }
   draw_int_operation_text(data, figures_y);
   al_draw_textf(glob.mid_font, al_map_rgb_f(0., 0., 0.), glob.zoom * 6.4, figures_y + glob.mid_font_size * 1.5,
      ALLEGRO_ALIGN_CENTRE, data->show_result_step >= 3 ? "(%d)" : "(?)", data->nbs[1] == 0 ? 0 : data->nbs[0] % data->nbs[1]);
}

void draw_vertical_int_operation(IntegerScreenData * data)
{
   float line_height = 1.5;
   for (int i = 0; i < data->nb_nbs; ++i) {
      float y = al_get_display_height(glob.disp) / 2. + glob.mid_font_size * line_height * (i - (data->nb_nbs + 1) / 2.);
      al_draw_text(glob.mid_font, al_map_rgb_f(0., 0., 0.), glob.zoom * 4.5, y, ALLEGRO_ALIGN_RIGHT, data->nbs_texts[i]);
      if (i >= 1) {
         al_draw_text(glob.mid_font, al_map_rgb_f(0., 0., 0.), glob.zoom * 3.1, y, ALLEGRO_ALIGN_LEFT, data->op_text);
      }
      if (data->cursor_pos == i) {
         draw_cursor(data->cursor_blinking_start, glob.zoom * 4.5, y);
      }
   }
   float result_y = al_get_display_height(glob.disp) / 2. + glob.mid_font_size * line_height * (data->nb_nbs / 2. - .5),
      line_y = result_y - glob.mid_font_size * (line_height - 1.) / 2.;
   al_draw_line(glob.zoom * 3.05, line_y, glob.zoom * 4.55, line_y, al_map_rgb_f(0., 0., 0.), glob.zoom * .02);
   al_draw_textf(glob.mid_font, al_map_rgb_f(0., 0., 0.), glob.zoom * 4.5, result_y,
      ALLEGRO_ALIGN_RIGHT, data->show_result_step >= 1 ? "%d" : "?", data->result);
}

void draw_int_laid_division(IntegerScreenData * data)
{
   float line_height = 1.5;
   al_draw_line(glob.zoom * 3.8, al_get_display_height(glob.disp) / 2. - glob.mid_font_size * line_height,
      glob.zoom * 3.8, al_get_display_height(glob.disp) / 2. + glob.mid_font_size * line_height,
      al_map_rgb_f(0., 0., 0.), glob.zoom * .02);
   al_draw_line(glob.zoom * 3.8, al_get_display_height(glob.disp) / 2., glob.zoom * 5., al_get_display_height(glob.disp) / 2.,
      al_map_rgb_f(0., 0., 0.), glob.zoom *.02);
   float first_line_y = al_get_display_height(glob.disp) / 2. - glob.mid_font_size * ((line_height - 1.) / 2. + 1.);
   al_draw_text(glob.mid_font, al_map_rgb_f(0., 0., 0.), glob.zoom * 3.75, first_line_y, ALLEGRO_ALIGN_RIGHT, data->nbs_texts[0]);
   al_draw_text(glob.mid_font, al_map_rgb_f(0., 0., 0.), glob.zoom * 4.95, first_line_y, ALLEGRO_ALIGN_RIGHT, data->nbs_texts[1]);
   float second_line_y = al_get_display_height(glob.disp) / 2. + glob.mid_font_size * (line_height - 1.) / 2.;
   al_draw_textf(glob.mid_font, al_map_rgb_f(0., 0., 0.), glob.zoom * 3.75, second_line_y, ALLEGRO_ALIGN_RIGHT,
      data->show_result_step >= 2 ? "%d" : "?", data->nbs[1] == 0 ? 0 : data->nbs[0] % data->nbs[1]);
   al_draw_textf(glob.mid_font, al_map_rgb_f(0., 0., 0.), glob.zoom * 4.95, second_line_y, ALLEGRO_ALIGN_RIGHT,
      data->show_result_step >= 1 ? "%d" : "?", data->result);
   if (data->cursor_pos >= 0) {
      draw_cursor(data->cursor_blinking_start, glob.zoom * (data->cursor_pos == 0 ? 3.75 : 4.95), first_line_y);
   }
}


void integers_operation_screen(IntegerScreenData * data, void (* draw_fct)(IntegerScreenData * data))
{
   Scrolling scroll;
   init_scrolling(&scroll);

   init_integers_data(data);
   data->show_grids = true;

   bool get_back = false, show_help = false;
   while (!glob.quit && !get_back) {
      scroll.max_x = ceilf(glob.zoom * 7.6);
      scroll.max_y = al_get_display_height(glob.disp);
      calibrate_scrolling(&scroll);

      al_clear_to_color(al_map_rgb_f(1., 1., 1.));
      draw_fct(data);
      draw_help_indication(2, &scroll);
      if (show_help) {
         draw_application_help(&scroll);
      }
      al_flip_display();

      ALLEGRO_EVENT evt;
      bool is_event_there = al_wait_for_event_timed(glob.evt_queue, &evt, .1);
      while (is_event_there) {
         if (show_help) {
            switch (evt.type) {
               case ALLEGRO_EVENT_KEY_CHAR:
               case ALLEGRO_EVENT_MOUSE_BUTTON_UP:
                  show_help = false;
                  break;
            }
         } else {
            switch (evt.type) {
               case ALLEGRO_EVENT_KEY_CHAR:
                  switch (evt.keyboard.keycode) {
                     case ALLEGRO_KEY_ESCAPE:
                        get_back = true;
                        break;
                     case ALLEGRO_KEY_F1:
                        show_help = true;
                        break;
                     case ALLEGRO_KEY_F2:
                        init_integers_data(data);
                        break;
                     case ALLEGRO_KEY_F3:
                        data->generate_negative = !data->generate_negative;
                        init_integers_data(data);
                        break;
                     case ALLEGRO_KEY_F4:
                        data->show_grids = !data->show_grids;
                        break;
                     case ALLEGRO_KEY_SPACE:
                        if (evt.keyboard.modifiers & ALLEGRO_KEYMOD_SHIFT) {
                           data->show_result_step = (data->show_result_step + data->nb_result_steps - 1) % data->nb_result_steps;
                        } else {
                           data->show_result_step = (data->show_result_step + 1) % data->nb_result_steps;
                        }
                        break;
                     case ALLEGRO_KEY_TAB:
                        apply_integer_value(data);
                        if (evt.keyboard.modifiers & ALLEGRO_KEYMOD_SHIFT) {
                           data->cursor_pos = (data->cursor_pos + data->nb_nbs - 1) % data->nb_nbs;
                        } else {
                           data->cursor_pos = (data->cursor_pos + 1) % data->nb_nbs;
                        }
                        data->cursor_blinking_start = al_get_time();
                        break;
                     case ALLEGRO_KEY_ENTER:
                        apply_integer_value(data);
                        data->cursor_pos = -1;
                        break;
                     default:
                        if (data->cursor_pos >= 0) {
                           char * modified_text = data->nbs_texts[data->cursor_pos];
                           int text_length = strlen(modified_text);
                           if (evt.keyboard.keycode == ALLEGRO_KEY_BACKSPACE) {
                              if (text_length > 0) {
                                 modified_text[text_length - 1] = '\0';
                              }
                           } else if (evt.keyboard.unichar == '-' && text_length == 0) {
                              strcpy(modified_text, "-");
                           } else if ('0' <= evt.keyboard.unichar && evt.keyboard.unichar <= '9' && text_length < 5) {
                              sprintf(modified_text + text_length, "%c", evt.keyboard.unichar);
                           }
                           data->cursor_blinking_start = al_get_time();
                        }
                  }
                  break;
            }
         }
         manage_global_events(evt, &scroll);
         is_event_there = al_get_next_event(glob.evt_queue, &evt);
      }
   }
}


void init_int_multi_operation_data(IntegerScreenData * data, int operations[])
{
   for (int i = 0; i < data->nb_nbs; ++i) {
      if (data->generate_negative) {
         data->nbs[i] = rand() * 19. / (RAND_MAX + 1.) - 9.;
      } else {
         data->nbs[i] = rand() * 9. / (RAND_MAX + 1.);
      }
   }
   for (int i = 0; i < data->nb_nbs - 1; ++i) {
      operations[i] = rand() * 3. / (RAND_MAX + 1.);
   }
   int (* operations_fcts[])(int nb1, int nb2) =
      { &int_addition_op, &int_substraction_op, &int_multiplication_op, &int_division_op };
   int nbs_calc[data->nb_nbs];
   memcpy(nbs_calc, data->nbs, sizeof(nbs_calc));
   int read_index, write_index = 0;
   for (read_index = 0; read_index < data->nb_nbs - 1; ++read_index) {
      if (operations[read_index] >= 2) {
         nbs_calc[write_index] = operations_fcts[operations[read_index]](nbs_calc[write_index], nbs_calc[read_index + 1]);
      } else {
         ++write_index;
         nbs_calc[write_index] = nbs_calc[read_index + 1];
      }
   }
   read_index = write_index = 0;
   for (int op_index = 0; op_index < data->nb_nbs - 1; ++op_index) {
      if (operations[op_index] <= 1) {
         nbs_calc[write_index] = operations_fcts[operations[op_index]](nbs_calc[write_index], nbs_calc[read_index + 1]);
         ++read_index;
      }
   }
   data->result = nbs_calc[0];
   data->show_result_step = 0;
}

void draw_int_multi_operation(IntegerScreenData * data, int operations[])
{
   char const * operations_texts[] = { "+", "−", "×", "÷" };
   char full_op_text[64] = "";
   int end_index = 0;
   for (int i = 0; i < data->nb_nbs; ++i) {
      if (i != 0) {
         end_index += sprintf(full_op_text + end_index, " %s ", operations_texts[operations[i - 1]]);
      }
      end_index += sprintf(full_op_text + end_index, data->nbs[i] < 0 ? "(%d)" : "%d", data->nbs[i]);
   }
   sprintf(full_op_text + end_index, data->show_result_step >= 1 ? " = %d" : " = ?", data->result);
   al_draw_text(glob.mid_font, al_map_rgb_f(0., 0., 0.),
      glob.zoom * 4., al_get_display_height(glob.disp) / 2. - glob.mid_font_size / 2., ALLEGRO_ALIGN_CENTRE, full_op_text);
}

void integers_multi_operation_screen()
{
   Scrolling scroll;
   init_scrolling(&scroll);

   IntegerScreenData screen_data, * data = &screen_data;
   data->nb_nbs = 5;
   data->nb_result_steps = 2;
   data->generate_negative = false;
   int operations[data->nb_nbs - 1];
   init_int_multi_operation_data(data, operations);

   bool get_back = false, show_help = false;
   while (!glob.quit && !get_back) {
      scroll.max_x = ceilf(glob.zoom * 8.);
      scroll.max_y = al_get_display_height(glob.disp);
      calibrate_scrolling(&scroll);

      al_clear_to_color(al_map_rgb_f(1., 1., 1.));
      draw_int_multi_operation(data, operations);
      draw_help_indication(2, &scroll);
      if (show_help) {
         draw_application_help(&scroll);
      }
      al_flip_display();

      ALLEGRO_EVENT evt;
      al_wait_for_event(glob.evt_queue, &evt);
      do {
         if (show_help) {
            switch (evt.type) {
               case ALLEGRO_EVENT_KEY_CHAR:
               case ALLEGRO_EVENT_MOUSE_BUTTON_UP:
                  show_help = false;
                  break;
            }
         } else {
            switch (evt.type) {
               case ALLEGRO_EVENT_KEY_CHAR:
                  switch (evt.keyboard.keycode) {
                     case ALLEGRO_KEY_ESCAPE:
                        get_back = true;
                        break;
                     case ALLEGRO_KEY_F1:
                        show_help = true;
                        break;
                     case ALLEGRO_KEY_F2:
                        init_int_multi_operation_data(data, operations);
                        break;
                     case ALLEGRO_KEY_F3:
                        data->generate_negative = !data->generate_negative;
                        init_int_multi_operation_data(data, operations);
                        break;
                     case ALLEGRO_KEY_SPACE:
                        if (evt.keyboard.modifiers & ALLEGRO_KEYMOD_SHIFT) {
                           data->show_result_step = (data->show_result_step + data->nb_result_steps - 1) % data->nb_result_steps;
                        } else {
                           data->show_result_step = (data->show_result_step + 1) % data->nb_result_steps;
                        }
                        break;
                  }
                  break;
            }
         }
         manage_global_events(evt, &scroll);
      } while (al_get_next_event(glob.evt_queue, &evt));
   }
}


MenuData integers_menu_data;

void draw_integers_menu_item(int item_index)
{
   char const * symbols[] = { "+", "−", "×", "÷", "+", "−", "×", "÷", "+", "=" };
   al_draw_text(glob.big_font, al_map_rgb_f(0., 0., 0.),
      integers_menu_data.items_width * (item_index * 2 + 1) / 2., al_get_display_height(glob.disp) / 2. - glob.big_font_size,
      ALLEGRO_ALIGN_CENTRE, symbols[item_index]);
}

void integers_menu()
{
   char const * menu_items_names[] = {
      "Addition jusqu’à 100",
      "Soustraction jusqu’à 100",
      "Multiplication jusqu’à 100",
      "Division jusqu’à 100",
      "Addition jusqu’à 10 000",
      "Soustraction jusqu’à 10 000",
      "Multiplication jusqu’à 10 000",
      "Division jusqu’à 10 000",
      "Addition de 5 nombres",
      "Priorité des opérateurs",
   };

   integers_menu_data.nb_items = sizeof(menu_items_names) / sizeof(*menu_items_names);
   integers_menu_data.items_names = menu_items_names;
   integers_menu_data.draw_item = &draw_integers_menu_item;
   init_scrolling(&integers_menu_data.scroll);
   integers_menu_data.cur_selected_item = -1;

   bool get_back = false, show_help = false;
   while (!glob.quit && !get_back) {
      integers_menu_data.items_width = glob.zoom * 2.;
      integers_menu_data.scroll.max_x = ceilf(integers_menu_data.nb_items * integers_menu_data.items_width);
      integers_menu_data.scroll.max_y = al_get_display_height(glob.disp);
      calibrate_scrolling(&integers_menu_data.scroll);

      al_clear_to_color(al_map_rgb_f(1., 1., 1.));
      draw_menu(integers_menu_data);
      draw_help_indication(1, &integers_menu_data.scroll);
      if (show_help) {
         draw_menu_help(&integers_menu_data.scroll);
      }
      al_flip_display();

      ALLEGRO_EVENT evt;
      al_wait_for_event(glob.evt_queue, &evt);
      do {
         if (show_help) {
            switch (evt.type) {
               case ALLEGRO_EVENT_KEY_CHAR:
               case ALLEGRO_EVENT_MOUSE_BUTTON_UP:
                  show_help = false;
                  break;
            }
         } else {
            switch (evt.type) {
               case ALLEGRO_EVENT_KEY_CHAR:
                  switch (evt.keyboard.keycode) {
                     case ALLEGRO_KEY_ESCAPE:
                        get_back = true;
                        break;
                     case ALLEGRO_KEY_F1:
                        integers_menu_data.cur_selected_item = -1;
                        show_help = true;
                        break;
                  }
                  break;
               case ALLEGRO_EVENT_MOUSE_BUTTON_UP:
                  if (evt.mouse.button == 1) {
                     switch (integers_menu_data.cur_selected_item) {
                        case 0: {
                           IntegerScreenData data = { &int_addition_op, "+", 2, 3, false, 50 };
                           integers_operation_screen(&data, &draw_int_addition);
                           break;
                        }
                        case 1: {
                           IntegerScreenData data = { &int_substraction_op, "−", 2, 3, false, 50 };
                           integers_operation_screen(&data, &draw_int_addition);
                           break;
                        }
                        case 2: {
                           IntegerScreenData data = { &int_multiplication_op, "×", 2, 3, false, 10 };
                           integers_operation_screen(&data, &draw_int_multiplication);
                           break;
                        }
                        case 3: {
                           IntegerScreenData data = { &int_division_op, "÷", 2, 4, false, 80, 10, 5 };
                           integers_operation_screen(&data, &draw_int_division);
                           break;
                        }
                        case 4: {
                           IntegerScreenData data = { &int_addition_op, "+", 2, 2, false, 5000 };
                           integers_operation_screen(&data, &draw_vertical_int_operation);
                           break;
                        }
                        case 5: {
                           IntegerScreenData data = { &int_substraction_op, "−", 2, 2, false, 5000 };
                           integers_operation_screen(&data, &draw_vertical_int_operation);
                           break;
                        }
                        case 6: {
                           IntegerScreenData data = { &int_multiplication_op, "×", 2, 2, false, 100 };
                           integers_operation_screen(&data, &draw_vertical_int_operation);
                           break;
                        }
                        case 7: {
                           IntegerScreenData data = { &int_division_op, "÷", 2, 3, false, 10000, 100, 2 };
                           integers_operation_screen(&data, &draw_int_laid_division);
                           break;
                        }
                        case 8: {
                           IntegerScreenData data = { &int_addition_op, "+", 5, 2, false, 100 };
                           integers_operation_screen(&data, &draw_vertical_int_operation);
                           break;
                        }
                        case 9:
                           integers_multi_operation_screen();
                           break;
                     }
                  }
                  break;
               case ALLEGRO_EVENT_MOUSE_AXES:
                  integers_menu_data.cur_selected_item =
                     (evt.mouse.x + integers_menu_data.scroll.dx) / integers_menu_data.items_width;
                  break;
            }
         }
         manage_global_events(evt, &integers_menu_data.scroll);
      } while (al_get_next_event(glob.evt_queue, &evt));
   }
}
