#include "global.h"
#include "menus.h"


void draw_menu(MenuData menu_data)
{
   if (0 <= menu_data.cur_selected_item && menu_data.cur_selected_item < menu_data.nb_items) {
      al_draw_filled_rounded_rectangle(
         menu_data.cur_selected_item * menu_data.items_width, al_get_display_height(glob.disp) * .1,
         (menu_data.cur_selected_item + 1) * menu_data.items_width, al_get_display_height(glob.disp) * .9,
         glob.zoom * .1, glob.zoom * .1, al_map_rgb_f(1., 1., .6));
   }
   for (int item_index = menu_data.scroll.dx < 0 ? 0 : menu_data.scroll.dx / menu_data.items_width;
      item_index < menu_data.nb_items &&
         item_index * menu_data.items_width - menu_data.scroll.dx < al_get_display_width(glob.disp);
      ++item_index) {
      menu_data.draw_item(item_index);
      al_draw_multiline_text(item_index == menu_data.cur_selected_item ? glob.bold_font : glob.font, al_map_rgb_f(0., 0., 0.),
         roundf(menu_data.items_width * (item_index * 2 + 1) / 2.),
         roundf(al_get_display_height(glob.disp) / 2. + glob.zoom * .2), menu_data.items_width - glob.zoom * .2,
         glob.font_size * 1.25, ALLEGRO_ALIGN_CENTRE, menu_data.items_names[item_index]);
   }
}


void draw_help(char const * lines[], int nb_lines, Scrolling const * scroll)
{
   int max_text_width = 0;
   for (int i = 0; i < nb_lines; ++i) {
      int text_width = al_get_text_width(glob.font, lines[i]);
      if (text_width > max_text_width) {
         max_text_width = text_width;
      }
   }
   float line_height = 1.5;
   al_draw_filled_rounded_rectangle(
      al_get_display_width(glob.disp) / 2. - max_text_width / 2. - glob.zoom * .3 + scroll->dx,
      al_get_display_height(glob.disp) / 2. - nb_lines * glob.font_size * line_height / 2. - glob.zoom * .3,
      al_get_display_width(glob.disp) / 2. + max_text_width / 2. + glob.zoom * .3 + scroll->dx,
      al_get_display_height(glob.disp) / 2. + nb_lines * glob.font_size * line_height / 2. + glob.zoom * .3,
      glob.zoom * .1, glob.zoom * .1, al_map_rgb_f(1., 1., .8));
   al_draw_rounded_rectangle(
      al_get_display_width(glob.disp) / 2. - max_text_width / 2. - glob.zoom * .3 + scroll->dx,
      al_get_display_height(glob.disp) / 2. - nb_lines * glob.font_size * line_height / 2. - glob.zoom * .3,
      al_get_display_width(glob.disp) / 2. + max_text_width / 2. + glob.zoom * .3 + scroll->dx,
      al_get_display_height(glob.disp) / 2. + nb_lines * glob.font_size * line_height / 2. + glob.zoom * .3,
      glob.zoom * .1, glob.zoom * .1, al_map_rgb_f(0., 0., 0.), glob.zoom * .02);
   for (int i = 0; i < nb_lines; ++i) {
      al_draw_text(glob.font, al_map_rgb_f(0., 0., 0.),
         al_get_display_width(glob.disp) / 2. - max_text_width / 2. + scroll->dx,
         al_get_display_height(glob.disp) / 2. + (i - nb_lines / 2.) * glob.font_size * line_height,
         ALLEGRO_ALIGN_LEFT, lines[i]);
   }
}


void draw_menu_help(Scrolling const * scroll)
{
   char const * lines[] = {
      "Clic gauche : Choisir un élément",
      "Clic droit : Défiler horizontalement",
      "Molette de la souris : Zoomer/dézoomer",
      "Échap : Retour",
      "F1 : Aide",
   };
   int nb_lines = sizeof(lines) / sizeof(*lines);
   draw_help(lines, nb_lines, scroll);
}


void draw_application_help(Scrolling const * scroll)
{
   char const * lines[] = {
      "Clic droit : Défiler horizontalement",
      "Molette de la souris : Zoomer/dézoomer",
      "Échap : Retour",
      "F1 : Aide",
      "F2 : Générer de nouveaux nombres",
      "F3 : Activer/désactiver la génération de nombres négatifs",
      "F4 : Afficher/masquer les graphiques",
      "Espace : Afficher le résultat suivant",
      "Maj+Espace : Masquer le résultat précédent",
      "Tab : Modifier le nombre suivant",
      "Maj+Tab : Modifier le nombre précédent",
      "Entrée : Valider la saisie",
   };
   int nb_lines = sizeof(lines) / sizeof(*lines);
   draw_help(lines, nb_lines, scroll);
}


void draw_help_indication(int number, Scrolling const * scroll)
{
   al_draw_textf(glob.font, al_map_rgb_f(0., 0., .5),
      glob.zoom * .04 + scroll->dx, al_get_display_height(glob.disp) - glob.font_size - glob.zoom * .04,
      ALLEGRO_ALIGN_LEFT, "F1 : Aide %d", number);
}
