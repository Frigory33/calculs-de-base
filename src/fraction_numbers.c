#include "global.h"
#include "menus.h"
#include "fraction_numbers.h"


float get_fraction_texts_width(char const * numerator, char const * denominator)
{
   int numerator_width = al_get_text_width(glob.mid_font, numerator),
      denominator_width = al_get_text_width(glob.mid_font, denominator),
      max_width = numerator_width > denominator_width ? numerator_width : denominator_width;
   return max_width + glob.zoom * .04;
}

float get_fraction_width(Fraction frac)
{
   char numerator_text[16], denominator_text[16];
   sprintf(numerator_text, "%d", frac.numerator);
   sprintf(denominator_text, "%d", frac.denominator);
   return get_fraction_texts_width(numerator_text, denominator_text);
}

void draw_fraction_texts(float x, float y, char const * numerator, char const * denominator, int cursor_pos,
   double cursor_blinking_start)
{
   float width = get_fraction_texts_width(numerator, denominator);
   al_draw_text(glob.mid_font, al_map_rgb_f(0., 0., 0.), x, y - glob.mid_font_size, ALLEGRO_ALIGN_CENTRE, numerator);
   if (cursor_pos == 0) {
      draw_cursor(cursor_blinking_start, x + al_get_text_width(glob.mid_font, numerator) / 2., y - glob.mid_font_size);
   }
   al_draw_line(x - width / 2., y + glob.mid_font_size / 2., x + width / 2., y + glob.mid_font_size / 2., al_map_rgb_f(0., 0., 0.),
      glob.zoom * .02);
   al_draw_text(glob.mid_font, al_map_rgb_f(0., 0., 0.), x, y + glob.mid_font_size * .75, ALLEGRO_ALIGN_CENTRE, denominator);
   if (cursor_pos == 1) {
      draw_cursor(cursor_blinking_start, x + al_get_text_width(glob.mid_font, denominator) / 2., y + glob.mid_font_size * .75);
   }
}

void draw_fraction(float x, float y, Fraction frac)
{
   float width = get_fraction_width(frac);
   al_draw_textf(glob.mid_font, al_map_rgb_f(0., 0., 0.), x, y - glob.mid_font_size, ALLEGRO_ALIGN_CENTRE, "%d", frac.numerator);
   al_draw_line(x - width / 2., y + glob.mid_font_size / 2., x + width / 2., y + glob.mid_font_size / 2., al_map_rgb_f(0., 0., 0.),
      glob.zoom * .02);
   al_draw_textf(glob.mid_font, al_map_rgb_f(0., 0., 0.), x, y + glob.mid_font_size * .75, ALLEGRO_ALIGN_CENTRE,
      "%d", frac.denominator);
}


void simplify_fraction(Fraction * frac, bool simplify_sign)
{
   int dividend = frac->numerator, divisor = frac->denominator, rest;
   if (dividend < divisor) {
      dividend = frac->denominator;
      divisor = frac->numerator;
   }
   do {
      rest = dividend % divisor;
      dividend = divisor;
      divisor = rest;
   } while (rest != 0);
   frac->numerator /= dividend;
   frac->denominator /= dividend;
   if (simplify_sign && ((frac->numerator < 0 && frac->denominator < 0) || (frac->numerator > 0 && frac->denominator < 0))) {
      frac->numerator *= -1;
      frac->denominator *= -1;
   }
}


typedef struct FractionScreenData FractionScreenData;
struct FractionScreenData {
   Fraction (* operation)(Fraction nb1, Fraction nb2);
   char const * op_text;
   int nb_nbs;
   bool generate_negative;
   int limit;

   Fraction nbs[2], result;
   char nbs_texts[2][8];
   bool show_result;
   int cursor_pos;
   double cursor_blinking_start;
};

void init_fractions_data(FractionScreenData * data)
{
   for (int i = 0; i < data->nb_nbs; ++i) {
      int * fraction_nbs[] = { &data->nbs[i].numerator, &data->nbs[i].denominator };
      for (int j = 0; j < 2; ++j) {
         int * nb = fraction_nbs[j];
         do {
            if (data->generate_negative) {
               *nb = rand() * (double)(data->limit * 2 + 1) / (RAND_MAX + 1.) - data->limit;
            } else {
               *nb = rand() * (double)(data->limit + 1) / (RAND_MAX + 1.);
            }
         } while (*nb == 0);
      }
      if (data->nb_nbs > 1) {
         simplify_fraction(&data->nbs[i], false);
      }
   }
   if (data->nb_nbs == 1) {
      int factor = rand() * 10. / (RAND_MAX + 1.) + 1.;
      data->nbs[0].numerator = data->nbs[0].numerator - data->nbs[0].numerator % factor;
      if (data->nbs[0].numerator == 0) {
         data->nbs[0].numerator += factor;
      }
      data->nbs[0].denominator = data->nbs[0].denominator - data->nbs[0].denominator % factor + factor;
      if (data->nbs[0].denominator == 0) {
         data->nbs[0].denominator += factor;
      }
      sprintf(data->nbs_texts[0], "%d", data->nbs[0].numerator);
      sprintf(data->nbs_texts[1], "%d", data->nbs[0].denominator);
   }
   data->result = data->nbs[0];
   for (int i = 1; i < data->nb_nbs; ++i) {
      data->result = data->operation(data->result, data->nbs[i]);
   }
   simplify_fraction(&data->result, true);
   data->show_result = false;
   data->cursor_pos = -1;
}

void apply_fraction_value(FractionScreenData * data)
{
   if (data->cursor_pos >= 0) {
      int * nb = data->cursor_pos == 0 ? &data->nbs[0].numerator : &data->nbs[0].denominator;
      *nb = atoi(data->nbs_texts[data->cursor_pos]);
      if (*nb == 0) {
         *nb = 1;
      }
      sprintf(data->nbs_texts[data->cursor_pos], "%d", *nb);
      data->result = data->nbs[0];
      simplify_fraction(&data->result, true);
   }
}


Fraction frac_addition_op(Fraction nb1, Fraction nb2)
{
   Fraction result = { nb1.numerator * nb2.denominator + nb2.numerator * nb1.denominator, nb1.denominator * nb2.denominator };
   return result;
}
Fraction frac_substraction_op(Fraction nb1, Fraction nb2)
{
   Fraction result = { nb1.numerator * nb2.denominator - nb2.numerator * nb1.denominator, nb1.denominator * nb2.denominator };
   return result;
}
Fraction frac_multiplication_op(Fraction nb1, Fraction nb2)
{
   Fraction result = { nb1.numerator * nb2.numerator, nb1.denominator * nb2.denominator };
   return result;
}
Fraction frac_division_op(Fraction nb1, Fraction nb2)
{
   Fraction result = { nb1.numerator * nb2.denominator, nb1.denominator * nb2.numerator };
   return result;
}


void draw_fraction_equality(FractionScreenData * data)
{
   float y = al_get_display_height(glob.disp) / 2. - glob.mid_font_size / 2.;
   draw_fraction_texts(glob.zoom * 2.8, y, data->nbs_texts[0], data->nbs_texts[1], data->cursor_pos, data->cursor_blinking_start);
   al_draw_text(glob.mid_font, al_map_rgb_f(0., 0., 0.), glob.zoom * 3.8, y, ALLEGRO_ALIGN_CENTRE, "=");
   if (data->show_result) {
      draw_fraction(glob.zoom * 4.8, y, data->result);
   } else {
      draw_fraction_texts(glob.zoom * 4.8, y, "?", "?", -1, 0.);
   }
}

void draw_fraction_operation(FractionScreenData * data)
{
   float y = al_get_display_height(glob.disp) / 2. - glob.mid_font_size / 2.;
   draw_fraction(glob.zoom * 1.8, y, data->nbs[0]);
   al_draw_text(glob.mid_font, al_map_rgb_f(0., 0., 0.), glob.zoom * 2.8, y, ALLEGRO_ALIGN_CENTRE, data->op_text);
   draw_fraction(glob.zoom * 3.8, y, data->nbs[1]);
   al_draw_text(glob.mid_font, al_map_rgb_f(0., 0., 0.), glob.zoom * 4.8, y, ALLEGRO_ALIGN_CENTRE, "=");
   if (data->show_result) {
      draw_fraction(glob.zoom * 5.8, y, data->result);
   } else {
      draw_fraction_texts(glob.zoom * 5.8, y, "?", "?", -1, 0.);
   }
}


void fractions_operation_screen(FractionScreenData * data, void (* draw_fct)(FractionScreenData * data))
{
   Scrolling scroll;
   init_scrolling(&scroll);

   init_fractions_data(data);

   bool get_back = false, show_help = false;
   while (!glob.quit && !get_back) {
      scroll.max_x = ceilf(glob.zoom * 7.6);
      scroll.max_y = al_get_display_height(glob.disp);
      calibrate_scrolling(&scroll);

      al_clear_to_color(al_map_rgb_f(1., 1., 1.));
      draw_fct(data);
      draw_help_indication(2, &scroll);
      if (show_help) {
         draw_application_help(&scroll);
      }
      al_flip_display();

      ALLEGRO_EVENT evt;
      bool is_event_there = al_wait_for_event_timed(glob.evt_queue, &evt, .1);
      while (is_event_there) {
         if (show_help) {
            switch (evt.type) {
               case ALLEGRO_EVENT_KEY_CHAR:
               case ALLEGRO_EVENT_MOUSE_BUTTON_UP:
                  show_help = false;
                  break;
            }
         } else {
            switch (evt.type) {
               case ALLEGRO_EVENT_KEY_CHAR:
                  switch (evt.keyboard.keycode) {
                     case ALLEGRO_KEY_ESCAPE:
                        get_back = true;
                        break;
                     case ALLEGRO_KEY_F1:
                        show_help = true;
                        break;
                     case ALLEGRO_KEY_F2:
                        init_fractions_data(data);
                        break;
                     case ALLEGRO_KEY_F3:
                        data->generate_negative = !data->generate_negative;
                        init_fractions_data(data);
                        break;
                     case ALLEGRO_KEY_SPACE:
                        data->show_result = !data->show_result;
                        break;
                     case ALLEGRO_KEY_TAB:
                        if (data->nb_nbs == 1) {
                           apply_fraction_value(data);
                           data->cursor_pos = (data->cursor_pos + 1) % 2;
                           data->cursor_blinking_start = al_get_time();
                        }
                        break;
                     case ALLEGRO_KEY_ENTER:
                        if (data->nb_nbs == 1) {
                           apply_fraction_value(data);
                           data->cursor_pos = -1;
                        }
                        break;
                     default:
                        if (data->nb_nbs == 1 && data->cursor_pos >= 0) {
                           char * modified_text = data->nbs_texts[data->cursor_pos];
                           int text_length = strlen(modified_text);
                           if (evt.keyboard.keycode == ALLEGRO_KEY_BACKSPACE) {
                              if (text_length > 0) {
                                 modified_text[text_length - 1] = '\0';
                              }
                           } else if (evt.keyboard.unichar == '-' && text_length == 0) {
                              strcpy(modified_text, "-");
                           } else if ('0' <= evt.keyboard.unichar && evt.keyboard.unichar <= '9' && text_length < 5) {
                              sprintf(modified_text + text_length, "%c", evt.keyboard.unichar);
                           }
                           data->cursor_blinking_start = al_get_time();
                        }
                  }
                  break;
            }
         }
         manage_global_events(evt, &scroll);
         is_event_there = al_get_next_event(glob.evt_queue, &evt);
      }
   }
}


MenuData fractions_menu_data;

void draw_fractions_menu_item(int item_index)
{
   char const * symbols[] = { "=", "+", "−", "×", "÷" };
   al_draw_text(glob.big_font, al_map_rgb_f(0., 0., 0.),
      fractions_menu_data.items_width * (item_index * 2 + 1) / 2., al_get_display_height(glob.disp) / 2. - glob.big_font_size,
      ALLEGRO_ALIGN_CENTRE, symbols[item_index]);
}

void fractions_menu()
{
   char const * menu_items_names[] = {
      "Simplification",
      "Addition",
      "Soustraction",
      "Multiplication",
      "Division",
   };

   fractions_menu_data.nb_items = sizeof(menu_items_names) / sizeof(*menu_items_names);
   fractions_menu_data.items_names = menu_items_names;
   fractions_menu_data.draw_item = &draw_fractions_menu_item;
   init_scrolling(&fractions_menu_data.scroll);
   fractions_menu_data.cur_selected_item = -1;

   bool get_back = false, show_help = false;
   while (!glob.quit && !get_back) {
      fractions_menu_data.items_width = glob.zoom * 2.;
      fractions_menu_data.scroll.max_x = ceilf(fractions_menu_data.nb_items * fractions_menu_data.items_width);
      fractions_menu_data.scroll.max_y = al_get_display_height(glob.disp);
      calibrate_scrolling(&fractions_menu_data.scroll);

      al_clear_to_color(al_map_rgb_f(1., 1., 1.));
      draw_menu(fractions_menu_data);
      draw_help_indication(1, &fractions_menu_data.scroll);
      if (show_help) {
         draw_menu_help(&fractions_menu_data.scroll);
      }
      al_flip_display();

      ALLEGRO_EVENT evt;
      al_wait_for_event(glob.evt_queue, &evt);
      do {
         if (show_help) {
            switch (evt.type) {
               case ALLEGRO_EVENT_KEY_CHAR:
               case ALLEGRO_EVENT_MOUSE_BUTTON_UP:
                  show_help = false;
                  break;
            }
         } else {
            switch (evt.type) {
               case ALLEGRO_EVENT_KEY_CHAR:
                  switch (evt.keyboard.keycode) {
                     case ALLEGRO_KEY_ESCAPE:
                        get_back = true;
                        break;
                     case ALLEGRO_KEY_F1:
                        fractions_menu_data.cur_selected_item = -1;
                        show_help = true;
                        break;
                  }
                  break;
               case ALLEGRO_EVENT_MOUSE_BUTTON_UP:
                  if (evt.mouse.button == 1) {
                     switch (fractions_menu_data.cur_selected_item) {
                        case 0: {
                           FractionScreenData data = { NULL, "", 1, false, 100 };
                           fractions_operation_screen(&data, &draw_fraction_equality);
                           break;
                        }
                        case 1: {
                           FractionScreenData data = { &frac_addition_op, "+", 2, false, 10 };
                           fractions_operation_screen(&data, &draw_fraction_operation);
                           break;
                        }
                        case 2: {
                           FractionScreenData data = { &frac_substraction_op, "−", 2, false, 10 };
                           fractions_operation_screen(&data, &draw_fraction_operation);
                           break;
                        }
                        case 3: {
                           FractionScreenData data = { &frac_multiplication_op, "×", 2, false, 10 };
                           fractions_operation_screen(&data, &draw_fraction_operation);
                           break;
                        }
                        case 4: {
                           FractionScreenData data = { &frac_division_op, "÷", 2, false, 10 };
                           fractions_operation_screen(&data, &draw_fraction_operation);
                           break;
                        }
                     }
                  }
                  break;
               case ALLEGRO_EVENT_MOUSE_AXES:
                  fractions_menu_data.cur_selected_item =
                     (evt.mouse.x + fractions_menu_data.scroll.dx) / fractions_menu_data.items_width;
                  break;
            }
         }
         manage_global_events(evt, &fractions_menu_data.scroll);
      } while (al_get_next_event(glob.evt_queue, &evt));
   }
}
