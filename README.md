Calculs de base
===============

Ce programme, réalisé en C avec la bibliothèque [Allegro 5](https://liballeg.org/), est destiné à des formateurs en arithmétique de base. Il propose des petites applications graphiques pour entrainer des élèves à calculer.

Ce projet a été réalisé à l’origine pour l’association [Actifs 33](https://www.actifs33.fr/).

Compilation et exécution
------------------------

Afin d’exécuter le programme, vous devez compiler les sources. Pour cela, vous avez besoin de 3 outils :
* GCC ;
* GNU Make ;
* Allegro 5 et ses extensions.

Sous GNU/Linux et macOS, GCC et Make sont probablement déjà présents. Il ne vous reste plus qu’à installer Allegro 5 et ses extensions, disponibles sous forme de paquets sous la plupart des distributions.

Sous Windows, je vous recommande d’utiliser l’environnement [MSYS](https://www.msys2.org/) pour installer les trois outils et les utiliser.

Pour compiler et exécuter le programme, rendez-vous dans le dossier `proj` avec la ligne de commande et entrez :
```sh
make run
```

Sous Windows, rajoutez de préférence l’information que vous compilez pour Windows 32 ou 64 bits :
```sh
make run TARGET=win64
```

Les 3 outils sont nécessaires pour compiler le programme, mais également pour l’exécuter ! Si vous voulez exécuter le programme compilé sur un système Windows sans avoir à installer MSYS dessus, vous pouvez : il faudra placer dans un même dossier le programme, les fichiers du dossier `data` et les nombreuses DLL fournies par MSYS et utilisées par le programme (quand vous lancez le programme sans avoir mis les DLL, il vous dit ce qu’il manque ; il faut 42 DLL en tout).
